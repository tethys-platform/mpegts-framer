mpegts-framer
=============

Splits mpegts stream into chunks each starting with the first packet of a video PES and containing full frame payload. Other PIDs are not considered when chunking.


Annotates payload with metadata, such as:

`i_frame: bool`
`pts: int64`


