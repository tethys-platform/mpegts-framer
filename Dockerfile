FROM redhat/ubi8:8.10 as build

RUN curl https://sh.rustup.rs -ssf | \
    sh -s -- --default-toolchain 1.82 -y &&\
    export PATH=/root/.cargo/bin:$PATH

RUN dnf install -y gcc python3 && dnf clean all && rm -rf /var/cache/dnf/*

WORKDIR /build/
COPY . /build/
RUN PATH=$PATH:/root/.cargo/bin cargo build --release

FROM redhat/ubi8-minimal:8.10
WORKDIR /app/

LABEL tethys.processor.name="mpegts-framer"
LABEL tethys.processor.summary="mpegts-framer: Splits mpegts stream into chunks each starting with the first packet of a video PES and containing full frame payload. Other PIDs are not considered when chunking."
LABEL tethys.processor.type="processor"
LABEL tethys.processor.stream=true

LABEL tethys.processor.property.strip_audio=false
LABEL tethys.processor.property.strip_audio.type=boolean
LABEL tethys.processor.property.strip_audio.required=false
LABEL tethys.processor.property.strip_audio.env=STRIP_AUDIO

LABEL tethys.processor.property.strip_data=false
LABEL tethys.processor.property.strip_data.type=boolean
LABEL tethys.processor.property.strip_data.required=false
LABEL tethys.processor.property.strip_data.env=STRIP_DATA
LABEL tethys.processor.dynamic_updates=false

COPY --from=build /build/target/release/mpegts-framer /app/

CMD ["/app/mpegts-framer"]

